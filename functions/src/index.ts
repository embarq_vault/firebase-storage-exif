import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
const os = require('os');
const path = require('path');
const { ExifImage } = require('exif');

const { serviceacc } = functions.config();

admin.initializeApp({
  credential: admin.credential.cert(serviceacc),
  databaseURL: "https://ionpizzapp.firebaseio.com",
  projectId: "ionpizzapp",
  storageBucket: "ionpizzapp.appspot.com",
});

// tslint:disable-next-line
(async () => {
  const tempLocalFile = path.join(os.tmpdir(), 'test-menu_san-remo.jpg');
  await admin.storage().bucket().file('test-menu/san-remo.jpg').download({
    destination: tempLocalFile
  });

  new ExifImage({ image: tempLocalFile }, (err, meta) => {
    if (err) {
      throw err;
    }

    console.log(tempLocalFile, JSON.stringify(meta, null, 2));
  })
})();
